<?php

	/*
	|	Checks all folders where this file is located for a folder called $dir
	| 	If it finds it then runs the 'git status' command
	|	If there are changes that needs to be commited then alert the status
	| 	If there are no changes, alert 'there are no new changes'
	|	If there are no $dir, then alert message
	*/

	$dir = '.git';
	$folder = array();
	$git = FALSE;
	$path = './';
	$totalGit = $noChanges = 0;

	/*
	|	Scans for directories
	*/

	if( $fileList = opendir($path) )
	{
		while( ($file = readdir($fileList)) !== FALSE )
		{
			if( is_dir($file) && $file !== '.' && $file !== '..' )
			{
				$folder[] = $file;
			}
		}
	}

	/*
	|	Scans each directory for $dir then run git status
	*/
	
	foreach($folder as $find)
	{
		$scan = scandir($find);
		if( in_array($dir,$scan) )
		{
			$git = TRUE;
			chdir($find);
			$pwd = exec('pwd');
			$status = exec('git status');
			$totalGit++;
	
			if( strpos($status,'nothing to commit') > -1 )
			{
				chdir('..');
				$noChanges++;
				continue;
			}
			
			echo $pwd;
			echo "\n";
			echo $status;
			echo "\n";
			chdir('..');
		}
	}

	/*
	| 	run if there are no new results
	*/

	if( $totalGit == $noChanges )
	{
		echo "There are no new changes to commit.\n";
	}

	/*
	|	there are no git repos
	*/

	if( !$git ) 
	{
		echo "No {$dir} directories found.\n";
	}

?>
