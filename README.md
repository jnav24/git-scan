Git Scan
========

A simple commandline script to check all git repositories in your development folder for changes that have not be added. This script runs a git status on all you git repos and only returns repos that has changes.

This is useful for those who have a lot of repos and lose track on which repos they needs to be added, committed and pushed.

Instructions
------------

Just download the script into the development directory where all your repos are located. In the terminal, cd into that directory and run:

	php scan.php

